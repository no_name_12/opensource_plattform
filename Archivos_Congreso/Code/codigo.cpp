#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#define _USE_MATH_DEFINES 
#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>  
#include "opencv2/core/core.hpp" 
#include <unistd.h> 
#include <chrono>
#include <time.h>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <opencv2/videoio/videoio_c.h>
#include "serialib.h"
#include <opencv2/imgcodecs.hpp>

#define PI 3.14159265

//LAS variables de medidas [metrs] y velocidad [metros/seg] ingresadas son las del prototipo (escaleadas 1:10), es decir, las reales o simuladas.
//Luego  dben ser multiplicadas x10 para llevar a un caso real

//ON-OFF
#define centrado_automatico 1.0 //0 apagado - 1 encendido
#define control_velocidad 1.0 
#define velocity 2 //en caso de control de velocidad apagado, se impone este valor de velocidad			// max motores 2.37 [km/h] - 
#define video_camara 1.0 //prende / apaga la entrada por cámara
#define saving 1.0 	//guarda un txt e imagenes
			
//CALIBRACIÓN - DATOS DE ENTRADA
#define depth_vision 3 //cantidad de autos por delante que deseamos ver (profundidad de vision) - 
#define retraso  30e-2//distancia [m] de retraso desde el punto x,y=128,255 de la imagen warpeada hasta el rear axis real del auto (debido al FOV de la cámara)

//PARÁMETROS FÍSICOS
#define medida_ruta 0.5//deberia ser 0.365 pero para este valor de 0.5 resulto andar bien//(valor normalizado en ARG, no cambia)
#define D_rueda 68e-3 //(ruedas físicas, no cambia)
#define max_delta 40 //angulo de giro máximo permitido 
	 
//VARIABLES DE SIMULACION
//ej: 10km/h reales se dividen por la escala 1:10 y luego por 3.6 para pasar a m/s
#define K_stanley 1.0//1.0 - 1.5 //parámetro de rigurosidad del seguimiento. recomendacion: 1,5 p/Ajuste lineal 
#define dT 50e-3 //período de sampleo

//PARÁMETROS del CÓDIGO
#define factor_altura_histograma 0.2  //franja horizontal de la imagen donde se aplica el histograma
#define ancho_cajas 20 //20
#define cantidad_cajas_apiladas 12 //8-12

#define rows 160//160-240-480 //cantidad de pixeles en x(filas)-y(columnas) de la imagen tomada (640x480)
#define cols 213//213-320-640

#define threshold_filtro 150//Recomendación: con lentes 150 - sin lentes 205 (le bajaria a 145)
#define factor_altura_puntos_ajuste_lineal 0.25 //el ajuste  sobre puntos de ruta se hace solo sobre los puntos con y>Y_lim. A mayor valor ingresado, mas alto en la imagen se fija. Recomendacion: 0.7 p/lineal - 0.2 p/cuadratico
#define factor_puntos_cercanos 0.2 //los puntos mas cercanos al auto se buscan en un rango limitado de la base de la imagen. Recomendación 0.2
#define canny_Threshold 168 //threshold del filtro tipo canny - a mayor valor mayor filtrado - Recomendación [170 - 180]
#define threshold_find_white 100 //threshold para la identificacion de puntos BLANCOS una vez filtrada la imagen - recomendacion CANNY 50 - FILTER 180
#define orden_polinomio 1
#define death_zone 105

//archivo de salida
#define ARCHIVO_NAME "draw.txt"
#define TIEMPO_CORRIDA 70 //150 //tiempo de ejcucion del codigo

int counter=0;
int ndatos=TIEMPO_CORRIDA/dT;   //guarda la cantidada de filas allocadas en memoria opara el archivo de datos de salida
    
using namespace cv;
using namespace std;

extern "C" {
    #include "funciones.c"
}

///////////////////////////////////////////////////////////////////////////////////
//FUNCIONES PARA EL PROCESAMIENTO DE LA IMAGEN
void sim_sensing(double* theta_e_list,double* dist_list,int* start_points, Mat &cameraFrame,Mat* imagenes_RAM,double* concavity) { 
	//propiedades de la imagen cruda tomada por la camara 
	if (counter==0){
		int rows_origen=640, cols_origen=480; 
		cout<<"R: Imagen tomada por la cámara: Filas: "<<rows_origen<<" Cols: "<<cols_origen<<"\n\n";   
	}	
	

	//Resize-ado
	Mat cameraFrame2; 
	resize(cameraFrame,cameraFrame2,Size(cols,rows));  
	
    int ok=image_process(start_points,theta_e_list,dist_list,cameraFrame2,imagenes_RAM,concavity);
	
    return;
}

void sim_actuation(serialib *serial,int* control,double* VEL_MEDIDA,double* VEL_REF,double* error, double* dist_list,double* theta_list,double* delta_list,double* error_int,double* concavity) {
	
	//MEDICIONES
	
    string data = readline(serial);
	
	int i=atoi(data.c_str() );
	double delta_time= (i) /6.0;
	//calculo de velocidad en km/h
	double rpm=0;
	if (delta_time>0){
		rpm= (60.0e3) / (20*delta_time);
	}
	double vel_medida=rpm*D_rueda*PI*3.6/60;
	
	VEL_MEDIDA[counter]=vel_medida;		//(km/h)
	//error en control de velocidad
	error[counter]= VEL_REF[counter] - VEL_MEDIDA[counter];
	error[counter]=FPB(error[counter],error,counter);
	
	//printf("R: %s",data.c_str());
	
	//CáLCULO DE ACCIONES DE CONTROL
	
	double delta_delantero=0;
	
	int accion_pwm_n=-death_zone;
	
	if (counter>100){ // damos un tiempo de  5 segundos para que se procesen las primeras imagenes
		
		delta_delantero=STANLEY(dist_list[counter],theta_list[counter],kmh2ms( VEL_REF[counter] ) ) * centrado_automatico; //USO LA VELOCIDAD DE REFERENCIA PORQUE ES UN VALOR MAS ESTABLE, AUNQUE RIGUROSAMENTE DEBIERA SER LA VEL MEDIDA
		//Nota: ACTUACION SOBRE SERVO 	-	angulo positvo implica que el auto gira a la izquierda
	
		accion_pwm_n=(int)( controlador_PI(error,error_int,control,counter)  *255);
		
		for (int k=40;k>0;k--){	//se perdieron las rutas
			if (concavity[counter-k]==-1.0){
			    accion_pwm_n=-death_zone;
			}
		}
	}
	else{
		error_int[counter]=0;
	}
	
	control[counter]=accion_pwm_n + death_zone; //guardo valor PWM en [0-255]
	delta_list[counter]=delta_delantero;
	

	//printf("R: --------------------\nR: ACTUATION\n");
	
	//COMUNICACION CON ARDUINO (PWM 255)
	
	string txt; //CONTIENE LOS COMANDOS DE ANGULO Y DE VELOCIDAD ORDENADOS A ARDUINO
	txt=to_string(int(delta_delantero))+"/"+to_string( control[counter] )+"\n";
        
	(*serial).writeString(txt.c_str());
	//printf("R: (delta/velocidad)= %s --------------\n",txt.c_str());
	
}

void main_loop(serialib *serial) {
       
    float dt = dT; //segundos
        
    double theta_e_list[ndatos]; //son los valores que debo entregarle al algoritmo de control Stanley
    double dist_list[ndatos];
    double delta_list[ndatos];
    double tiempo_list[ndatos];

    double VEL_MEDIDA[ndatos];
    double VEL_REF[ndatos];
    double error[ndatos];
    double error_int[ndatos];
    
    double concavity[ndatos];
    
    int accion_control[ndatos]; //PWM    [0-1]
    
    int start_points[2]; //guarda la posicion del pixel de inicio de la linea izq y derecha (en ese orden) en variables x-y
    
    Mat imagenes_RAM[3*(TIEMPO_CORRIDA+5)]; //aqui guardaré una imagen por segundo para la imagen cruda, la filtrada y la imagen final de control
    
    unsigned t0, t1;
    t0=clock();
    
    auto comienzo = std::chrono::high_resolution_clock::now();
    
    double t_prev=0.0;
    double t_prox=0.0;
    
    while (t_prox<TIEMPO_CORRIDA) {
    	auto begin = std::chrono::high_resolution_clock::now();
    	auto begin_time = std::chrono::duration_cast<std::chrono::nanoseconds>(begin-comienzo); //tiempo de partida de iteracion de 1 imagen
	    double t_start = begin_time.count() * 1e-9; //tiempo de inicio de iteracion i en segundos


		tiempo_list[counter]=t_start; 
		
		if (counter%20==0 && counter>0){
		    printf("%d ) Ts=%f \n",counter/20,tiempo_list[counter]-tiempo_list[counter-1]);
		}
		
	Mat frame;
	
	if (video_camara==1){
		frame = getImage();
		if(!frame.data) break;
	}
	else{
		int val=30+counter*3;
		frame=imread("./recorrido2/cruda/cruda_"+std::to_string(val)+".png");
	}
	
		
	sim_sensing(theta_e_list,dist_list,start_points,frame,imagenes_RAM,concavity); 
	//aqui ocurre el procesamioento de la imagen. Mide distancia, angulo de ruta.
	//hacer esta funcion que devuelva una estructua con los valores para mas simplicidad del codigo
	
	//podriamos pensar en una futura funcion que en funcion del sensado de la imagen establezca una velocidad de referencia.
	VEL_REF[counter]=set_reference_velocity(counter,concavity);
	
	sim_actuation(serial,accion_control,VEL_MEDIDA,VEL_REF,error,dist_list,theta_e_list,delta_list,error_int,concavity);
		//se comunica con arduino
		//mide velocidad
		//calcula delta segun el metodo empleado: stanley por el momento
		//actua sobre servo
		//actua sobre motores
		
	
	auto end = std::chrono::high_resolution_clock::now();
	auto end_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - comienzo); //tiempo de finalizaciopn de iteracion de la imagen
	
	double time_after = end_time.count() * 1e-9; //tiempo de finalizacion de la iteracion i
			
	double elap_time=time_after-t_start;//lapso de tiempo incurrido en la iteracion i
	
	//printf("R: dt : %.3f seconds.\n", elap_time);
		
	t_prox=(counter+1)*dt; //tiempo final teorico de la iteracion i
    
    int tobed;
	if ( time_after<t_prox){//
	    tobed=(int)((t_prox-time_after) * 1e6 );
		usleep( tobed ); //microsegundos
		//printf("%f\n",elap_time);
	}         
	
	counter++;	
	}
	
	//SALIDA DEL LOOP
	//Se imprimen los vectores de variables de la corrida
	//Se imprimen las imagenes de la corrida
	string namefile=ARCHIVO_NAME; 
        
	if (saving==1.0){
		impresor(ndatos,theta_e_list,delta_list,tiempo_list,dist_list,namefile,accion_control,VEL_MEDIDA,VEL_REF,error,concavity);
		impresor_imagenes(imagenes_RAM,3*(TIEMPO_CORRIDA));
	}
	
	printf("----------------    END ------------------\n");
}

///////////////////////////////////////////////////////////////////////////////////
 
int main(int argc,char* argv[]) {
	printf("Program started\n");
	
	initCamara();
	
	//CONEXION PUERTO SERIE
	// Serial object
	serialib serial;

	// Connection to serial port
	char errorOpening = serial.openDevice("/dev/ttyACM0", 115200);

	// If connection fails, return the error code otherwise, display a success message
	if (errorOpening!=1){
		return errorOpening;
	}
    
	printf ("Successful connection\n");
	sleep(5); //espera de 5 segundos 
	serial.writeString("reset\n");

	clear_buffer(&serial);
	printf("Buffer cleared\n");
	
	//PROCESAMIENTO DE IMAGEN
	main_loop(&serial);

	// Close the serial device
	serial.closeDevice();
	
	printf("------------- END ----------------\n");
	
	return 0;
}
